package server

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"go.uber.org/mock/gomock"
)

var (
	_ modserver.Factory = (*Factory)(nil)
)

func TestServer_GetServerInfos(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	mockRPCAPI := mock_modserver.NewMockAgentRPCAPI(ctrl)
	ctx := modshared.InjectRPCAPI(context.Background(), mockRPCAPI)
	s := &server{
		info: &server_info.ServerInfo{
			Version:  "any-version",
			BuildRef: "any-build-ref",
		},
	}

	// WHEN
	req := &rpc.GetServerInfoRequest{}
	resp, err := s.GetServerInfo(ctx, req)

	// THEN
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.NotNil(t, resp.CurrentServerInfo)
	assert.Equal(t, "any-version", resp.CurrentServerInfo.Version)
	assert.Equal(t, "any-build-ref", resp.CurrentServerInfo.BuildRef)
}
