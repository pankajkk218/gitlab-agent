package server

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info/rpc"
)

type server struct {
	rpc.UnimplementedServerInfoServer

	info *server_info.ServerInfo
}

func (s *server) GetServerInfo(ctx context.Context, _ *rpc.GetServerInfoRequest) (*rpc.GetServerInfoResponse, error) {
	return &rpc.GetServerInfoResponse{
		CurrentServerInfo: s.info,
	}, nil
}
