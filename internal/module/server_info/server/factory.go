package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info/rpc"
)

type Factory struct{}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	rpc.RegisterServerInfoServer(config.APIServer, &server{
		info: &server_info.ServerInfo{
			Version:  config.Version,
			BuildRef: config.CommitID,
		},
	})

	return nil, nil
}

func (f *Factory) Name() string {
	return server_info.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
