package server

import (
	"strings"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

const (
	defaultKubernetesAPIListenNetwork    = "tcp"
	defaultKubernetesAPIListenAddress    = "0.0.0.0:8154"
	defaultListenGracePeriod             = 5 * time.Second
	defaultAllowedAgentInfoCacheTTL      = 1 * time.Minute
	defaultAllowedAgentInfoCacheErrorTTL = 10 * time.Second
	defaultShutdownGracePeriod           = 1 * time.Hour
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	prototool.NotNil(&config.Agent)
	o := config.Agent.KubernetesApi

	if o == nil {
		return
	}
	prototool.NotNil(&o.Listen)
	prototool.StringPtr(&o.Listen.Network, defaultKubernetesAPIListenNetwork)
	prototool.String(&o.Listen.Address, defaultKubernetesAPIListenAddress)
	prototool.Duration(&o.Listen.ListenGracePeriod, defaultListenGracePeriod)
	prototool.Duration(&o.Listen.ShutdownGracePeriod, defaultShutdownGracePeriod)
	if !strings.HasSuffix(o.UrlPathPrefix, "/") {
		o.UrlPathPrefix = o.UrlPathPrefix + "/"
	}
	prototool.Duration(&o.AllowedAgentCacheTtl, defaultAllowedAgentInfoCacheTTL)
	prototool.Duration(&o.AllowedAgentCacheErrorTtl, defaultAllowedAgentInfoCacheErrorTTL)
}
