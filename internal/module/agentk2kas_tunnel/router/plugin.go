package router

import (
	"context"
	"errors"
	"strconv"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"go.opentelemetry.io/otel/attribute"
	otelcodes "go.opentelemetry.io/otel/codes"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"k8s.io/utils/clock"
)

var (
	_ tunserver.RouterPlugin = (*Plugin)(nil)
)

const (
	// routingAgentIDMetadataKey is used to pass destination agent id in request metadata
	// from the routing kas instance, that is handling the incoming request, to the gateway kas instance,
	// that is forwarding the request to an agentk.
	routingAgentIDMetadataKey = tunserver.RoutingHopPrefix + "routing-agent-id"

	routingDurationMetricName                = "tunnel_routing_duration"
	routingTunnelTimeoutConnectedRecently    = "tunnel_routing_timeout_connected_recently_total"
	routingTunnelTimeoutNotConnectedRecently = "tunnel_routing_timeout_not_connected_recently_total"

	routingStatusAttributeName attribute.Key = "status"
)

var (
	routingStatusSuccessAttrSet = attribute.NewSet(routingStatusAttributeName.String("success"))
	routingStatusAbortedAttrSet = attribute.NewSet(routingStatusAttributeName.String("aborted"))
)

type AgentFinder interface {
	AgentLastConnected(ctx context.Context, agentID int64) (time.Time, error)
}

type TunnelFinder interface {
	FindTunnel(ctx context.Context, agentID int64, service, method string) (bool, tunserver.FindHandle)
}

// gatewayFinderFactory proves an indirection point for testing.
type gatewayFinderFactory func(outgoingCtx context.Context, log *zap.Logger, fullMethod string, agentID int64) tunserver.GatewayFinder

type Plugin struct {
	api                                       modshared.API
	agentFinder                               AgentFinder
	tunnelRegistry                            TunnelFinder
	tracer                                    trace.Tracer
	tunnelFindTimeout                         time.Duration
	clock                                     clock.PassiveClock
	gwFactory                                 gatewayFinderFactory
	routingDuration                           otelmetric.Float64Histogram
	routingTimeoutConnectedRecentlyCounter    otelmetric.Int64Counter
	routingTimeoutNotConnectedRecentlyCounter otelmetric.Int64Counter
}

func NewPlugin(
	api modshared.API,
	kasPool grpctool.PoolInterface,
	gatewayQuerier tunserver.PollingGatewayURLQuerier,
	agentFinder AgentFinder,
	tunnelRegistry TunnelFinder,
	tracer trace.Tracer,
	meter otelmetric.Meter,
	ownPrivateAPIURL string,
	pollConfig retry.PollConfigFactory,
	tryNewGatewayInterval time.Duration,
	tunnelFindTimeout time.Duration,
) (*Plugin, error) {
	return newPlugin(
		api,
		agentFinder,
		tunnelRegistry,
		tracer,
		meter,
		tunnelFindTimeout,
		clock.RealClock{},
		func(outgoingCtx context.Context, log *zap.Logger, method string, agentID int64) tunserver.GatewayFinder {
			return tunserver.NewGatewayFinder(
				outgoingCtx,
				log,
				kasPool,
				gatewayQuerier,
				api,
				method,
				ownPrivateAPIURL,
				agentID,
				pollConfig,
				tryNewGatewayInterval,
			)
		})
}

func newPlugin(
	api modshared.API,
	agentFinder AgentFinder,
	tunnelRegistry TunnelFinder,
	tracer trace.Tracer,
	meter otelmetric.Meter,
	tunnelFindTimeout time.Duration,
	clock clock.PassiveClock,
	gwFactory gatewayFinderFactory,
) (*Plugin, error) {
	routingDuration, connectedTimeoutCounter, registeredTimeoutCounter, err := constructRoutingMetrics(meter)
	if err != nil {
		return nil, err
	}

	return &Plugin{
		api:                                    api,
		agentFinder:                            agentFinder,
		tunnelRegistry:                         tunnelRegistry,
		tracer:                                 tracer,
		tunnelFindTimeout:                      tunnelFindTimeout,
		clock:                                  clock,
		gwFactory:                              gwFactory,
		routingDuration:                        routingDuration,
		routingTimeoutConnectedRecentlyCounter: connectedTimeoutCounter,
		routingTimeoutNotConnectedRecentlyCounter: registeredTimeoutCounter,
	}, nil
}

func (p *Plugin) FindReadyGateway(ctx context.Context, log *zap.Logger, method string) (tunserver.ReadyGateway, *zap.Logger, int64, error) {
	startRouting := p.clock.Now()
	findCtx, span := p.tracer.Start(ctx, "router.findReadyGateway", trace.WithSpanKind(trace.SpanKindInternal))
	defer span.End()

	md, _ := metadata.FromOutgoingContext(ctx)
	agentID, err := agentIDFromMeta(md)
	if err != nil {
		span.SetStatus(otelcodes.Error, "")
		span.RecordError(err)
		return tunserver.ReadyGateway{}, nil, 0, err // returns gRPC status error
	}

	log = log.With(logz.AgentID(agentID))
	gf := p.gwFactory(ctx, log, method, agentID)
	findCtx, findCancel := context.WithTimeout(findCtx, p.tunnelFindTimeout)
	defer findCancel()

	rg, err := gf.Find(findCtx)
	if err != nil {
		switch { // Order is important here.
		case ctx.Err() != nil: // Incoming stream canceled.
			p.routingDuration.Record( //nolint: contextcheck
				context.Background(),
				float64(p.clock.Since(startRouting))/float64(time.Second),
				otelmetric.WithAttributeSet(routingStatusAbortedAttrSet),
			)
			span.SetStatus(otelcodes.Error, "Aborted")
			span.RecordError(ctx.Err())
			return tunserver.ReadyGateway{}, nil, 0, grpctool.StatusErrorFromContext(ctx, "request aborted")
		case findCtx.Err() != nil: // Find tunnel timed out.
			findCtxErr := findCtx.Err()

			errCtx, cancel := context.WithTimeout(context.WithoutCancel(findCtx), 1*time.Second) // preserve context values for tracing
			defer cancel()
			// NOTE: we know at this point that we failed to find a tunnel within the timeout.
			// However, we don't know if the agent is simply not connected or if we fail to find the gateway.
			// We have the chance here to correlate the error with the data from the agent registration.
			lastConnected, lastConnErr := p.agentFinder.AgentLastConnected(errCtx, agentID)
			if lastConnErr != nil {
				p.api.HandleProcessingError(findCtx, log, "Unable to correlate tunnel timeout error with agent registration data", lastConnErr, fieldz.AgentID(agentID))
				// we can continue here as we still want to report the original error.
			} else if lastConnected != (time.Time{}) {
				// we determined that the agent is registered and should be connected, but yet we failed to find the gateway for it.
				p.routingTimeoutConnectedRecentlyCounter.Add(context.Background(), 1) //nolint: contextcheck
				span.SetStatus(otelcodes.Error, "Timed out, agent connected recently")
				span.RecordError(findCtxErr)

				lastConnAgo := p.clock.Since(lastConnected) / time.Second
				p.api.HandleProcessingError(findCtx, log, "Finding tunnel timed out", errors.New("agent was recently connected"),
					fieldz.AgentID(agentID), fieldz.LastConnectedAt(lastConnected), fieldz.SecondsAgo(lastConnAgo))
				return tunserver.ReadyGateway{}, nil, 0, status.Errorf(codes.DeadlineExceeded, "finding tunnel timed out. Agent last connected %d second(s) ago", lastConnAgo)
			}
			p.routingTimeoutNotConnectedRecentlyCounter.Add(context.Background(), 1) //nolint: contextcheck
			span.SetStatus(otelcodes.Error, "Timed out, agent not connected recently")
			span.RecordError(findCtxErr)

			p.api.HandleProcessingError(findCtx, log, "Finding tunnel timed out. Agent hasn't connected recently", errors.New(findCtxErr.Error()), fieldz.AgentID(agentID))
			return tunserver.ReadyGateway{}, nil, 0, status.Error(codes.DeadlineExceeded, "finding tunnel timed out. Agent hasn't connected recently. Make sure the agent is connected and up to date")
		default: // This should never happen, but let's handle a non-ctx error for completeness and future-proofing.
			span.SetStatus(otelcodes.Error, "Failed")
			span.RecordError(err)

			p.api.HandleProcessingError(findCtx, log, "Finding tunnel failed", err, fieldz.AgentID(agentID))
			return tunserver.ReadyGateway{}, nil, 0, status.Errorf(codes.Unavailable, "find tunnel failed: %v", err)
		}
	}
	p.routingDuration.Record( //nolint: contextcheck
		context.Background(),
		float64(p.clock.Since(startRouting))/float64(time.Second),
		otelmetric.WithAttributeSet(routingStatusSuccessAttrSet),
	)
	span.SetStatus(otelcodes.Ok, "")
	return rg, log, agentID, nil
}

func (p *Plugin) FindTunnel(stream grpc.ServerStream, rpcAPI modshared.RPCAPI) (bool, *zap.Logger, tunserver.FindHandle, error) {
	ctx := stream.Context()
	md, _ := metadata.FromIncomingContext(ctx)
	agentID, err := agentIDFromMeta(md)
	if err != nil {
		return false, nil, nil, err
	}
	sts := grpc.ServerTransportStreamFromContext(ctx)
	service, method := grpctool.SplitGRPCMethod(sts.Method())
	log := rpcAPI.Log().With(logz.AgentID(agentID))
	found, handle := p.tunnelRegistry.FindTunnel(ctx, agentID, service, method)
	return found, log, handle, nil
}

func SetRoutingMetadata(md metadata.MD, agentID int64) metadata.MD {
	if md == nil {
		md = metadata.MD{}
	}
	md[routingAgentIDMetadataKey] = []string{strconv.FormatInt(agentID, 10)}
	return md
}

// agentIDFromMeta returns agent ID or an gRPC status error.
func agentIDFromMeta(md metadata.MD) (int64, error) {
	val := md.Get(routingAgentIDMetadataKey)
	if len(val) != 1 {
		return 0, status.Errorf(codes.InvalidArgument, "expecting a single %s, got %d", routingAgentIDMetadataKey, len(val))
	}
	agentID, err := strconv.ParseInt(val[0], 10, 64)
	if err != nil {
		return 0, status.Errorf(codes.InvalidArgument, "invalid %s", routingAgentIDMetadataKey)
	}
	return agentID, nil
}

func constructRoutingMetrics(dm otelmetric.Meter) (otelmetric.Float64Histogram, otelmetric.Int64Counter, otelmetric.Int64Counter, error) {
	hist, err := dm.Float64Histogram(
		routingDurationMetricName,
		otelmetric.WithUnit("s"),
		otelmetric.WithDescription("The time it takes the tunnel router to find a suitable tunnel in seconds"),
		otelmetric.WithExplicitBucketBoundaries(0.001, 0.004, 0.016, 0.064, 0.256, 1.024, 4.096, 16.384),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	routingTimeoutConnectedRecentlyCounter, err := dm.Int64Counter(
		routingTunnelTimeoutConnectedRecently,
		otelmetric.WithDescription("The total number of times routing timed out for agents that recently connected but no connection was found to use for the tunnel"),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	routingTimeoutNotConnectedRecentlyCounter, err := dm.Int64Counter(
		routingTunnelTimeoutNotConnectedRecently,
		otelmetric.WithDescription("The total number of times routing timed out for agents that haven't been connected recently"),
	)
	if err != nil {
		return nil, nil, nil, err
	}
	return hist, routingTimeoutConnectedRecentlyCounter, routingTimeoutNotConnectedRecentlyCounter, nil
}
