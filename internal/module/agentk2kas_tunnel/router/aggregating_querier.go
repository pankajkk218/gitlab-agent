package router

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type pollingState byte

const (
	stopped pollingState = iota
	running
)

type pollingContext struct {
	mu        sync.Mutex                    // protects fields below
	subs      syncz.Subscriptions[[]string] // sub/unsub are protected for consistency, but Dispatch() is not - syncz.Subscriptions has its own mutex.
	cancel    context.CancelFunc
	kasURLs   []string
	stoppedAt time.Time
	state     pollingState
}

func newPollingContext() *pollingContext {
	return &pollingContext{
		state: stopped,
	}
}

func (c *pollingContext) setKASURLs(kasURLs []string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.kasURLs = kasURLs
}

func (c *pollingContext) isExpired(before time.Time) bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.state == stopped && c.stoppedAt.Before(before)
}

// AggregatingQuerier groups polling requests.
type AggregatingQuerier struct {
	log        *zap.Logger
	delegate   Querier
	api        modshared.API
	tracer     trace.Tracer
	pollConfig retry.PollConfigFactory
	gcPeriod   time.Duration

	mu        sync.Mutex
	listeners map[int64]*pollingContext
}

func NewAggregatingQuerier(log *zap.Logger, delegate Querier, api modshared.API, tracer trace.Tracer, pollConfig retry.PollConfigFactory, gcPeriod time.Duration) *AggregatingQuerier {
	return &AggregatingQuerier{
		log:        log,
		delegate:   delegate,
		api:        api,
		tracer:     tracer,
		pollConfig: pollConfig,
		gcPeriod:   gcPeriod,
		listeners:  make(map[int64]*pollingContext),
	}
}

func (q *AggregatingQuerier) Run(ctx context.Context) error {
	done := ctx.Done()
	t := time.NewTicker(q.gcPeriod)
	defer t.Stop()
	for {
		select {
		case <-done:
			return nil
		case <-t.C:
			q.runGC()
		}
	}
}

func (q *AggregatingQuerier) runGC() {
	before := time.Now().Add(-q.gcPeriod)
	q.mu.Lock()
	defer q.mu.Unlock()
	for agentID, pc := range q.listeners {
		if pc.isExpired(before) {
			delete(q.listeners, agentID)
		}
	}
}

func (q *AggregatingQuerier) PollGatewayURLs(ctx context.Context, agentID int64, cb tunserver.PollGatewayURLsCallback) {
	listen := q.maybeStartPolling(ctx, agentID)
	defer q.maybeStopPolling(agentID)

	listen(func(ctx context.Context, kasURLs []string) {
		cb(kasURLs)
	})
}

func (q *AggregatingQuerier) CachedGatewayURLs(agentID int64) []string {
	q.mu.Lock()
	defer q.mu.Unlock()
	pc := q.listeners[agentID]
	if pc == nil { // no existing context
		return nil
	}
	pc.mu.Lock()
	defer pc.mu.Unlock()
	return pc.kasURLs
}

func (q *AggregatingQuerier) maybeStartPolling(ctx context.Context, agentID int64) syncz.Listen[[]string] {
	q.mu.Lock()
	defer q.mu.Unlock()
	pc := q.listeners[agentID]
	if pc == nil { // no existing context
		pc = newPollingContext()
		q.listeners[agentID] = pc
	}
	return q.registerConsumerLocked(ctx, agentID, pc)
}

func (q *AggregatingQuerier) registerConsumerLocked(ctx context.Context, agentID int64, pc *pollingContext) syncz.Listen[[]string] {
	pc.mu.Lock()
	defer pc.mu.Unlock()

	// Subscribe for notifications. Must be done before starting a goroutine, so we don't miss any notifications.
	listen := pc.subs.Subscribe(ctx)

	switch pc.state {
	case stopped:
		pc.state = running
		pollCtx, cancel := context.WithCancel(context.Background())
		pc.cancel = cancel
		go q.poll(pollCtx, agentID, pc) //nolint: contextcheck
	case running:
		// Already polling, nothing to do.
	default:
		panic(fmt.Sprintf("invalid State value: %d", pc.state))
	}
	return listen
}

func (q *AggregatingQuerier) maybeStopPolling(agentID int64) {
	q.mu.Lock()
	defer q.mu.Unlock()

	pc := q.listeners[agentID]
	if q.unregisterConsumerLocked(pc) {
		// No point in keeping this pollingContext around if it doesn't have any cached URLs.
		delete(q.listeners, agentID)
	}
}

func (q *AggregatingQuerier) unregisterConsumerLocked(pc *pollingContext) bool {
	pc.mu.Lock()
	defer pc.mu.Unlock()

	if pc.subs.Len() == 0 {
		pc.cancel()     // stop polling
		pc.cancel = nil // release the kraken! err... GC
		pc.state = stopped
		pc.stoppedAt = time.Now()
		return len(pc.kasURLs) == 0
	}
	return false
}

func (q *AggregatingQuerier) poll(ctx context.Context, agentID int64, pc *pollingContext) {
	ctx, span := q.tracer.Start(ctx, "AggregatingQuerier.poll",
		trace.WithSpanKind(trace.SpanKindInternal),
		trace.WithAttributes(api.TraceAgentIDAttr.Int64(agentID)),
	)
	defer span.End()

	_ = retry.PollWithBackoff(ctx, q.pollConfig(), func(ctx context.Context) (error, retry.AttemptResult) {
		kasURLs, err := q.delegate.KASURLsByAgentID(ctx, agentID)
		if err != nil {
			q.api.HandleProcessingError(ctx, q.log, "KASURLsByAgentID() failed", err, fieldz.AgentID(agentID))
			// fallthrough
		}
		if len(kasURLs) > 0 {
			pc.subs.Dispatch(ctx, kasURLs)
		}
		if err != nil && len(kasURLs) == 0 {
			// if there was an error, and we failed to retrieve any kas URLs from Redis, we don't want to erase the
			// cache. So, no-op here.
		} else {
			pc.setKASURLs(kasURLs)
		}
		return nil, retry.Continue
	})
}
