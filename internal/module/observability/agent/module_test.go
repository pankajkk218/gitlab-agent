package agent

import "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"

var (
	_ modagent.Module = &module{}
)
