package server

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type server struct {
	rpc.UnimplementedAgentTrackerServer
	agentQuerier agent_tracker.Querier
}

func (s *server) GetConnectedAgentsByProjectIDs(ctx context.Context, req *rpc.GetConnectedAgentsByProjectIDsRequest) (*rpc.GetConnectedAgentsByProjectIDsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()
	var infos agent_tracker.ConnectedAgentInfoCollector
	for _, projectID := range req.ProjectIds {
		err := s.agentQuerier.GetConnectionsByProjectID(ctx, projectID, infos.Collect)
		if err != nil {
			rpcAPI.HandleProcessingError(log, "GetConnectionsByProjectID() failed", err)
			return nil, status.Error(codes.Unavailable, "GetConnectionsByProjectID() failed")
		}
	}
	return &rpc.GetConnectedAgentsByProjectIDsResponse{
		Agents: infos,
	}, nil
}

func (s *server) GetConnectedAgentsByAgentIDs(ctx context.Context, req *rpc.GetConnectedAgentsByAgentIDsRequest) (*rpc.GetConnectedAgentsByAgentIDsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()
	var infos agent_tracker.ConnectedAgentInfoCollector
	for _, agentID := range req.AgentIds {
		err := s.agentQuerier.GetConnectionsByAgentID(ctx, agentID, infos.Collect)
		if err != nil {
			rpcAPI.HandleProcessingError(log, "GetConnectionsByAgentID() failed", err)
			return nil, status.Error(codes.Unavailable, "GetConnectionsByAgentID() failed")
		}
	}
	return &rpc.GetConnectedAgentsByAgentIDsResponse{
		Agents: infos,
	}, nil
}

func (s *server) CountAgentsByAgentVersions(ctx context.Context, _ *rpc.CountAgentsByAgentVersionsRequest) (*rpc.CountAgentsByAgentVersionsResponse, error) {
	rpcAPI := modshared.RPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	counts, err := s.agentQuerier.CountAgentsByAgentVersions(ctx)
	if err != nil {
		rpcAPI.HandleProcessingError(log, "CountAgentsByAgentVersions() failed", err)
		return nil, status.Error(codes.Unavailable, "CountAgentsByAgentVersions() failed")
	}

	return &rpc.CountAgentsByAgentVersionsResponse{
		AgentVersions: counts,
	}, nil
}
