package agent_registrar //nolint:stylecheck

import "time"

const (
	ModuleName              = "agent_registrar"
	RegisterAttemptInterval = 5 * time.Minute
)
