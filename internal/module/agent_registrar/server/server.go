package server

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type server struct {
	rpc.UnimplementedAgentRegistrarServer
	agentRegisterer agent_tracker.ExpiringRegisterer
}

func (s *server) Register(ctx context.Context, req *rpc.RegisterRequest) (*rpc.RegisterResponse, error) {
	rpcAPI := modserver.AgentRPCAPIFromContext(ctx)
	log := rpcAPI.Log()

	// Get agent info
	agentInfo, err := rpcAPI.AgentInfo(ctx, log)
	if err != nil {
		return nil, err
	}

	connectedAgentInfo := &agent_tracker.ConnectedAgentInfo{
		AgentMeta:    req.AgentMeta,
		ConnectedAt:  timestamppb.Now(),
		ConnectionId: req.PodId,
		AgentId:      agentInfo.ID,
		ProjectId:    agentInfo.ProjectID,
	}

	// Register agent
	err = s.agentRegisterer.RegisterExpiring(ctx, connectedAgentInfo)
	if err != nil {
		rpcAPI.HandleProcessingError(log, "Failed to register agent", err, fieldz.AgentID(agentInfo.ID))
		return nil, status.Error(codes.Unavailable, "failed to register agent")
	}

	return &rpc.RegisterResponse{}, nil
}
