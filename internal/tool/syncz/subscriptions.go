package syncz

import (
	"context"
	"sync"
)

type EventCallback[E any] func(ctx context.Context, e E)

type sub[E any] struct {
	done <-chan struct{}
	ch   chan<- E
}

type Subscriptions[E any] struct {
	mu   sync.Mutex
	subs map[*sub[E]]struct{}
}

func (s *Subscriptions[E]) add(sb *sub[E]) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.subs == nil {
		s.subs = make(map[*sub[E]]struct{})
	}
	s.subs[sb] = struct{}{}
}

func (s *Subscriptions[E]) remove(sb *sub[E]) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.subs, sb)
}

type Listen[E any] func(cb EventCallback[E])

// Subscribe subscribes to events but, unlike On, does not start listening for the immediately.
// It returns a function that must be called to receive events. Make sure the function is always called as it
// performs cleanup once the passed context is done.
// The returned function must only be called once.
func (s *Subscriptions[E]) Subscribe(ctx context.Context) Listen[E] {
	ch := make(chan E)
	done := ctx.Done()
	sb := &sub[E]{
		done: done,
		ch:   ch,
	}
	s.add(sb)
	return func(cb EventCallback[E]) {
		defer s.remove(sb)

		for {
			select {
			case <-done:
				return
			case e := <-ch:
				cb(ctx, e)
			}
		}
	}
}

func (s *Subscriptions[E]) On(ctx context.Context, cb EventCallback[E]) {
	s.Subscribe(ctx)(cb)
}

// Len returns the number of subscriptions.
func (s *Subscriptions[E]) Len() int {
	s.mu.Lock()
	defer s.mu.Unlock()

	return len(s.subs)
}

// Dispatch dispatches the given event to all added subscriptions.
func (s *Subscriptions[E]) Dispatch(ctx context.Context, e E) {
	done := ctx.Done()

	s.mu.Lock()
	defer s.mu.Unlock()

	for sb := range s.subs {
		select {
		case <-done:
			return
		case <-sb.done:
			// It doesn't want the events anymore.
		case sb.ch <- e:
		}
	}
}
