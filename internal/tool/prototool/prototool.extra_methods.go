package prototool

import (
	"net/http"
	"net/url"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
)

func (x *HttpRequest) HTTPHeader() http.Header {
	return ValuesMapToHTTPHeader(x.Header)
}

func (x *HttpRequest) URLQuery() url.Values {
	return ValuesMapToURLValues(x.Query)
}

func (x *HttpRequest) IsUpgrade() bool {
	return x.Header[httpz.UpgradeHeader] != nil
}

func (x *HttpResponse) HTTPHeader() http.Header {
	return ValuesMapToHTTPHeader(x.Header)
}

func ValuesMapToHTTPHeader(from map[string]*Values) http.Header {
	res := make(http.Header, len(from))
	for key, val := range from {
		res[key] = val.Value
	}
	return res
}

func HTTPHeaderToValuesMap(from http.Header) map[string]*Values {
	if len(from) == 0 {
		return nil
	}
	res := make(map[string]*Values, len(from))
	for key, val := range from {
		res[key] = &Values{
			Value: val,
		}
	}
	return res
}

func URLValuesToValuesMap(from url.Values) map[string]*Values {
	if len(from) == 0 {
		return nil
	}
	res := make(map[string]*Values, len(from))
	for key, val := range from {
		res[key] = &Values{
			Value: val,
		}
	}
	return res
}

func ValuesMapToURLValues(from map[string]*Values) url.Values {
	query := make(url.Values, len(from))
	for key, val := range from {
		query[key] = val.Value
	}
	return query
}
