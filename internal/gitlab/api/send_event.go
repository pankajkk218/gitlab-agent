package api

import (
	"context"
	"net/http"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
)

const (
	EventAPIPath = "/api/v4/internal/kubernetes/agent_events"
)

type EventData struct {
	Events map[string][]Event `json:"events"`
}

type Event struct {
	UserID    int64 `json:"user_id"`
	ProjectID int64 `json:"project_id"`
}

func SendEvent(ctx context.Context, client gitlab.ClientInterface, data EventData, opts ...gitlab.DoOption) error {
	return client.Do(ctx,
		joinOpts(opts,
			gitlab.WithMethod(http.MethodPost),
			gitlab.WithPath(EventAPIPath),
			gitlab.WithJSONRequestBody(data),
			gitlab.WithResponseHandler(gitlab.NoContentResponseHandler()),
			gitlab.WithJWT(true),
		)...,
	)
}
