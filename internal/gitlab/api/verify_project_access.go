package api

import (
	"context"
	"net/url"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
)

const (
	VerifyProjectAccessAPIPath = "/api/v4/internal/kubernetes/verify_project_access"
)

func VerifyProjectAccess(ctx context.Context, client gitlab.ClientInterface, agentToken api.AgentToken, projectID string, opts ...gitlab.DoOption) (bool, error) {
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithPath(VerifyProjectAccessAPIPath),
			gitlab.WithQuery(url.Values{
				ProjectIDQueryParam: []string{projectID},
			}),
			gitlab.WithAgentToken(agentToken),
			gitlab.WithResponseHandler(gitlab.NoContentResponseHandler()),
			gitlab.WithJWT(true),
		)...,
	)
	if err == nil {
		return true, nil
	}

	if gitlab.IsForbidden(err) || gitlab.IsUnauthorized(err) || gitlab.IsNotFound(err) {
		return false, nil
	}

	return false, err
}
