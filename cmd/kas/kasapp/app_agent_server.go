package kasapp

import (
	"context"
	"crypto/tls"
	"net"
	"time"

	"github.com/ash2k/stager"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"github.com/redis/rueidis"
	agentk2kas_router "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/router"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/metric"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/nettool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/tlstool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/wstunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"nhooyr.io/websocket"
)

const (
	defaultMaxMessageSize = 10 * 1024 * 1024
)

type agentServer struct {
	log            *zap.Logger
	listenCfg      *kascfg.ListenAgentCF
	tlsConfig      *tls.Config
	server         *grpc.Server
	tunnelRegistry *agentk2kas_router.Registry
	auxCancel      context.CancelFunc
	ready          func()
}

func newAgentServer(log *zap.Logger, ot *obsTools, cfg *kascfg.ConfigurationFile, srvAPI modserver.API,
	redisClient rueidis.Client, factory modserver.AgentRPCAPIFactory,
	ownPrivateAPIURL string, grpcServerErrorReporter grpctool.ServerErrorReporter) (*agentServer, error) {
	listenCfg := cfg.Agent.Listen
	tlsConfig, err := tlstool.MaybeDefaultServerTLSConfig(listenCfg.CertificateFile, listenCfg.KeyFile)
	if err != nil {
		return nil, err
	}
	// Tunnel registry
	tracker, err := agentk2kas_router.NewRedisTracker(redisClient, cfg.Redis.KeyPrefix+":tunnel_tracker2", ownPrivateAPIURL, ot.meter, log)
	if err != nil {
		return nil, err
	}
	tunnelRegistry := agentk2kas_router.NewRegistry(
		log,
		srvAPI,
		ot.tracer,
		cfg.Agent.RedisConnInfoRefresh.AsDuration(),
		cfg.Agent.RedisConnInfoGc.AsDuration(),
		cfg.Agent.RedisConnInfoTtl.AsDuration(),
		tracker,
	)
	var agentConnectionLimiter grpctool.ServerLimiter
	agentConnectionLimiter = redistool.NewTokenLimiter(
		redisClient,
		cfg.Redis.KeyPrefix+":agent_limit",
		uint64(listenCfg.ConnectionsPerTokenPerMinute),
		func(ctx context.Context) redistool.RPCAPI {
			return &tokenLimiterAPI{
				rpcAPI: modserver.AgentRPCAPIFromContext(ctx),
			}
		},
	)
	agentConnectionLimiter, err = metric.NewAllowLimiterInstrumentation(
		"agent_connection",
		float64(listenCfg.ConnectionsPerTokenPerMinute),
		"{connection/token/m}",
		ot.tracer,
		ot.meter,
		agentConnectionLimiter,
	)
	if err != nil {
		return nil, err
	}
	auxCtx, auxCancel := context.WithCancel(context.Background())
	traceContextProp := propagation.TraceContext{} // only want trace id, not baggage from external clients/agents
	keepaliveOpt, sh := grpctool.MaxConnectionAge2GRPCKeepalive(auxCtx, listenCfg.MaxConnectionAge.AsDuration())
	serverOpts := []grpc.ServerOption{
		grpc.StatsHandler(otelgrpc.NewServerHandler(
			otelgrpc.WithTracerProvider(ot.tp),
			otelgrpc.WithMeterProvider(ot.mp),
			otelgrpc.WithPropagators(traceContextProp),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		grpc.StatsHandler(ot.ssh),
		grpc.StatsHandler(sh),
		grpc.SharedWriteBuffer(true),
		grpc.ChainStreamInterceptor(
			ot.streamProm, // 1. measure all invocations
			modserver.StreamAgentRPCAPIInterceptor(factory), // 2. inject RPC API
			grpc_validator.StreamServerInterceptor(),        // x. wrap with validator
			grpctool.StreamServerLimitingInterceptor(agentConnectionLimiter),
			grpctool.StreamServerErrorReporterInterceptor(grpcServerErrorReporter),
		),
		grpc.ChainUnaryInterceptor(
			ot.unaryProm, // 1. measure all invocations
			modserver.UnaryAgentRPCAPIInterceptor(factory), // 2. inject RPC API
			grpc_validator.UnaryServerInterceptor(),        // x. wrap with validator
			grpctool.UnaryServerLimitingInterceptor(agentConnectionLimiter),
			grpctool.UnaryServerErrorReporterInterceptor(grpcServerErrorReporter),
		),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             20 * time.Second,
			PermitWithoutStream: true,
		}),
		keepaliveOpt,
	}

	if !listenCfg.Websocket && tlsConfig != nil {
		// If we are listening for WebSocket connections, gRPC server doesn't need TLS as it's handled by the
		// HTTP/WebSocket server. Otherwise, we handle it here (if configured).
		serverOpts = append(serverOpts, grpc.Creds(credentials.NewTLS(tlsConfig)))
	}

	return &agentServer{
		log:            log,
		listenCfg:      listenCfg,
		tlsConfig:      tlsConfig,
		server:         grpc.NewServer(serverOpts...),
		tunnelRegistry: tunnelRegistry,
		auxCancel:      auxCancel,
		ready:          ot.probeRegistry.RegisterReadinessToggle("agentServer"),
	}, nil
}

func (s *agentServer) Start(stage stager.Stage) {
	registryCtx, registryCancel := context.WithCancel(context.Background())
	stage.Go(func(ctx context.Context) error {
		return s.tunnelRegistry.Run(registryCtx) // use a separate ctx to stop when the server starts stopping
	})
	grpctool.StartServer(stage, s.server, func() (retLis net.Listener, retErr error) {
		defer func() {
			if retErr != nil { // something went wrong here, stop the registry
				registryCancel()
			}
		}()
		var lis net.Listener
		var err error
		if s.listenCfg.Websocket { // Explicitly handle TLS for a WebSocket server
			if s.tlsConfig != nil {
				s.tlsConfig.NextProtos = []string{httpz.TLSNextProtoH2, httpz.TLSNextProtoH1} // h2 for gRPC, http/1.1 for WebSocket
				lis, err = nettool.TLSListenWithOSTCPKeepAlive(*s.listenCfg.Network, s.listenCfg.Address, s.tlsConfig)
			} else {
				lis, err = nettool.ListenWithOSTCPKeepAlive(*s.listenCfg.Network, s.listenCfg.Address)
			}
			if err != nil {
				return nil, err
			}
			wsWrapper := wstunnel.ListenerWrapper{
				AcceptOptions: websocket.AcceptOptions{
					CompressionMode: websocket.CompressionDisabled,
				},
				// TODO set timeouts
				ReadLimit:  defaultMaxMessageSize,
				ServerName: kasServerName(),
			}
			lis = wsWrapper.Wrap(lis, s.tlsConfig != nil)
		} else {
			lis, err = nettool.ListenWithOSTCPKeepAlive(*s.listenCfg.Network, s.listenCfg.Address)
			if err != nil {
				return nil, err
			}
		}
		addr := lis.Addr()
		s.log.Info("Agentk API endpoint is up",
			logz.NetNetworkFromAddr(addr),
			logz.NetAddressFromAddr(addr),
			logz.IsWebSocket(s.listenCfg.Websocket),
		)

		s.ready()

		return lis, nil
	}, func() {
		time.Sleep(s.listenCfg.ListenGracePeriod.AsDuration()) // This delays closing the listener.
		// We first want gRPC server to send GOAWAY and only then return from the RPC handlers.
		// So we delay signaling the handlers and registry.
		// See https://github.com/grpc/grpc-go/issues/6830 for more background.
		// Start a goroutine in a second and...
		time.AfterFunc(time.Second, func() {
			s.auxCancel()    // ... signal running RPC handlers to stop.
			registryCancel() // ... signal registry to stop.
		})
	})
}
