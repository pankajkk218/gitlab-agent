package kasapp

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/ash2k/stager"
	"github.com/getsentry/sentry-go"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/redis/rueidis"
	"github.com/redis/rueidis/rueidisotel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly/vendored/client"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api"
	agent_configuration_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_configuration/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar"
	agent_registrar_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_registrar/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	agent_tracker_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker/server"
	agentk2kas_router "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/router"
	agentk2kas_tunnel_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agentk2kas_tunnel/server"
	configuration_project_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/configuration_project/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/event_tracker"
	event_tracker_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/event_tracker/server"
	flux_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux/server"
	gitlab_access_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/gitlab_access/server"
	google_profiler_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/google_profiler/server"
	kubernetes_api_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	notifications_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/notifications/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability"
	observability_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability/server"
	server_info_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/server_info/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/usage_metrics"
	usage_metrics_server "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/usage_metrics/server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/cache"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/httpz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/metric"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/tlstool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	promexp "go.opentelemetry.io/otel/exporters/prometheus"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/propagation"
	metricsdk "go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.25.0"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
	"go.uber.org/zap"
	"golang.org/x/time/rate"
	"google.golang.org/grpc"
	_ "google.golang.org/grpc/encoding/gzip" // Install the gzip compressor
	"google.golang.org/grpc/stats"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	routingAttemptInterval   = 50 * time.Millisecond
	routingInitBackoff       = 100 * time.Millisecond
	routingMaxBackoff        = 1 * time.Second
	routingResetDuration     = 10 * time.Second
	routingBackoffFactor     = 2.0
	routingJitter            = 1.0
	routingTunnelFindTimeout = 20 * time.Second
	routingCachePeriod       = 5 * time.Minute
	routingTryNewKASInterval = 10 * time.Millisecond

	authSecretLength = 32

	kasName = "gitlab-kas"

	kasTracerName = "kas"
	kasMeterName  = "kas"

	gitlabBuildInfoGaugeMetricName               = "gitlab_build_info"
	kasVersionAttr                 attribute.Key = "version"
)

type obsTools struct {
	probeRegistry    *observability.ProbeRegistry
	reg              *prometheus.Registry
	tracer           trace.Tracer
	meter            otelmetric.Meter
	tp               trace.TracerProvider
	mp               otelmetric.MeterProvider
	p                propagation.TextMapPropagator
	ssh              stats.Handler
	csh              stats.Handler
	streamProm       grpc.StreamServerInterceptor
	unaryProm        grpc.UnaryServerInterceptor
	streamClientProm grpc.StreamClientInterceptor
	unaryClientProm  grpc.UnaryClientInterceptor
}

type ConfiguredApp struct {
	Log           *zap.Logger
	Configuration *kascfg.ConfigurationFile
}

var _ tracesdk.SpanExporter = (*TraceDualWriter)(nil)

type TraceDualWriter struct {
	legacyExporter tracesdk.SpanExporter
	mainExporter   tracesdk.SpanExporter
}

func (tdw *TraceDualWriter) ExportSpans(ctx context.Context, spans []tracesdk.ReadOnlySpan) error {
	var err1 error

	if tdw.legacyExporter != nil {
		err1 = tdw.legacyExporter.ExportSpans(ctx, spans)
	}
	err2 := tdw.mainExporter.ExportSpans(ctx, spans)

	return errors.Join(err1, err2)
}

func (tdw *TraceDualWriter) Shutdown(ctx context.Context) error {
	var err1 error

	if tdw.legacyExporter != nil {
		err1 = tdw.legacyExporter.Shutdown(ctx)
	}
	err2 := tdw.mainExporter.Shutdown(ctx)

	return errors.Join(err1, err2)
}

func (a *ConfiguredApp) Run(ctx context.Context) (retErr error) {
	a.Log.Sugar().Infof("Running KAS %s", kasServerName())

	ot, stop, err := a.constructObservabilityTools(ctx)
	if err != nil {
		return err
	}
	defer errz.SafeCall(stop, &retErr)

	// GitLab REST client
	gitLabClient, err := a.constructGitLabClient(ot) //nolint: contextcheck
	if err != nil {
		return err
	}

	// Sentry
	sentryHub, err := a.constructSentryHub(ot)
	if err != nil {
		return fmt.Errorf("error tracker: %w", err)
	}

	// Redis
	redisClient, err := a.constructRedisClient(ot)
	if err != nil {
		return fmt.Errorf("redis client: %w", err)
	}
	defer redisClient.Close()
	ot.probeRegistry.RegisterReadinessProbe("redis", constructRedisReadinessProbe(redisClient))

	// Agent tracker
	agentTracker, err := a.constructAgentTracker(ErrReporterWithoutAgentInfos(sentryHub), redisClient, ot.meter)
	if err != nil {
		return err
	}

	agentInfoResolver := constructAgentInfoResolver(agentTracker)

	srvAPI := newServerAPI(a.Log, sentryHub, redisClient, agentInfoResolver)

	errRep := modshared.APIToErrReporter(srvAPI)
	grpcServerErrorReporter := &serverErrorReporter{log: a.Log, errReporter: errRep}

	// RPC API factory
	rpcAPIFactory, agentRPCAPIFactory := a.constructRPCAPIFactory(errRep, sentryHub, gitLabClient, redisClient, ot.tracer, agentInfoResolver)

	// Server for handling API requests from other kas instances
	privateAPISrv, err := newPrivateAPIServer(a.Log, ot, errRep, a.Configuration, rpcAPIFactory, grpcServerErrorReporter) //nolint: contextcheck
	if err != nil {
		return fmt.Errorf("private API server: %w", err)
	}
	defer errz.SafeClose(privateAPISrv, &retErr)

	// Server for handling agentk requests
	agentSrv, err := newAgentServer(a.Log, ot, a.Configuration, srvAPI, redisClient, agentRPCAPIFactory, //nolint: contextcheck
		privateAPISrv.ownURL, grpcServerErrorReporter)
	if err != nil {
		return fmt.Errorf("agent server: %w", err)
	}

	// Server for handling external requests e.g. from GitLab
	apiSrv, err := newAPIServer(a.Log, ot, a.Configuration, rpcAPIFactory, grpcServerErrorReporter) //nolint: contextcheck
	if err != nil {
		return fmt.Errorf("API server: %w", err)
	}

	// Kas to agentk router
	pollConfig := retry.NewPollConfigFactory(routingAttemptInterval, retry.NewExponentialBackoffFactory(
		routingInitBackoff,
		routingMaxBackoff,
		routingResetDuration,
		routingBackoffFactor,
		routingJitter,
	))
	gatewayQuerier := agentk2kas_router.NewAggregatingQuerier(a.Log, agentSrv.tunnelRegistry, srvAPI, ot.tracer, pollConfig, routingCachePeriod)
	routerPlugin, err := agentk2kas_router.NewPlugin(
		srvAPI,
		privateAPISrv.kasPool,
		gatewayQuerier,
		constructAgentFinder(agentTracker),
		agentSrv.tunnelRegistry,
		ot.tracer,
		ot.meter,
		privateAPISrv.ownURL,
		pollConfig,
		routingTryNewKASInterval,
		routingTunnelFindTimeout,
	)
	if err != nil {
		return err
	}
	kasToAgentRouter := &tunserver.Router{
		Plugin:           routerPlugin,
		PrivateAPIServer: privateAPISrv.server,
	}

	// Gitaly client
	gitalyClientPool, err := a.constructGitalyPool(ot) //nolint: contextcheck
	if err != nil {
		return err
	}
	defer errz.SafeClose(gitalyClientPool, &retErr)

	// kas->agentk connection pool
	agentPool := newAgentConnPool(&tunserver.RoutingClientConn{
		Log:    a.Log,
		API:    srvAPI,
		Plugin: routerPlugin,
	})

	// Usage tracker
	usageTracker := usage_metrics.NewUsageTracker()

	// Event tracker
	eventTracker := event_tracker.NewEventTracker()

	// Module factories
	factories := []modserver.Factory{
		&observability_server.Factory{
			Gatherer:   ot.reg,
			Registerer: ot.reg,
		},
		&google_profiler_server.Factory{},
		&agent_configuration_server.Factory{},
		&configuration_project_server.Factory{},
		&notifications_server.Factory{
			PublishEvent:      srvAPI.publishEvent,
			SubscribeToEvents: srvAPI.subscribeToEvents,
		},
		&flux_server.Factory{},
		&usage_metrics_server.Factory{
			UsageTracker: usageTracker,
		},
		&event_tracker_server.Factory{
			EventTracker: eventTracker,
		},
		&gitlab_access_server.Factory{},
		&agent_registrar_server.Factory{
			AgentRegisterer: agentTracker,
		},
		&agent_tracker_server.Factory{
			AgentQuerier: agentTracker,
		},
		&agentk2kas_tunnel_server.Factory{
			TunnelHandler: agentSrv.tunnelRegistry,
		},
		&kubernetes_api_server.Factory{},
		&server_info_server.Factory{},
	}

	// Construct modules
	poolWrapper := &gitaly.Pool{
		ClientPool: gitalyClientPool,
	}
	var beforeServersModules, afterServersModules []modserver.Module
	for _, factory := range factories {
		// factory.New() must be called from the main goroutine because it may mutate a gRPC server (register an API)
		// and that can only be done before Serve() is called on the server.
		moduleName := factory.Name()
		moduleLog := a.Log.With(logz.ModuleName(moduleName))
		module, err := factory.New(&modserver.Config{
			Log:              moduleLog,
			API:              srvAPI,
			Config:           a.Configuration,
			GitLabClient:     gitLabClient,
			UsageTracker:     usageTracker,
			EventTracker:     eventTracker,
			AgentServer:      agentSrv.server,
			APIServer:        apiSrv.server,
			RegisterAgentAPI: kasToAgentRouter.RegisterTunclientAPI,
			AgentConnPool:    agentPool.Get,
			Gitaly:           poolWrapper,
			TraceProvider:    ot.tp,
			TracePropagator:  ot.p,
			MeterProvider:    ot.mp,
			Meter:            ot.meter,
			RedisClient:      redisClient,
			KASName:          kasName,
			Version:          cmd.Version,
			CommitID:         cmd.Commit,
			ProbeRegistry:    ot.probeRegistry,
		})
		if err != nil {
			return fmt.Errorf("%s: %w", moduleName, err)
		}
		if module == nil {
			moduleLog.Debug("Module is not started, because the factory did not create it")
			continue
		}
		phase := factory.StartStopPhase()
		switch phase {
		case modshared.ModuleStartBeforeServers:
			beforeServersModules = append(beforeServersModules, module)
		case modshared.ModuleStartAfterServers:
			afterServersModules = append(afterServersModules, module)
		default:
			return fmt.Errorf("invalid StartStopPhase from factory %s: %d", moduleName, phase)
		}
	}

	// Start things up. Stages are shut down in reverse order.
	return stager.RunStages(ctx,
		// Start things that modules use.
		func(stage stager.Stage) {
			stage.Go(agentTracker.Run)
			stage.Go(gatewayQuerier.Run)
		},
		// Start modules.
		func(stage stager.Stage) {
			startModules(stage, beforeServersModules)
		},
		// Start other gRPC servers.
		func(stage stager.Stage) { //nolint:contextcheck
			agentSrv.Start(stage)
			apiSrv.Start(stage)
			privateAPISrv.Start(stage)
		},
		// Start modules.
		func(stage stager.Stage) {
			startModules(stage, afterServersModules)
		},
	)
}

func (a *ConfiguredApp) constructRPCAPIFactory(errRep errz.ErrReporter, sentryHub *sentry.Hub, gitLabClient gitlab.ClientInterface, redisClient rueidis.Client, dt trace.Tracer, agentInfoResolver modserver.AgentInfoResolver) (modshared.RPCAPIFactory, modserver.AgentRPCAPIFactory) {
	aCfg := a.Configuration.Agent
	f := serverRPCAPIFactory{
		log:               a.Log,
		sentryHub:         sentryHub,
		agentInfoResolver: agentInfoResolver,
	}
	fAgent := serverAgentRPCAPIFactory{
		rpcAPIFactory: f.New,
		gitLabClient:  gitLabClient,
		agentInfoCache: cache.NewWithError[api.AgentToken, *api.AgentInfo](
			aCfg.InfoCacheTtl.AsDuration(),
			aCfg.InfoCacheErrorTtl.AsDuration(),
			&redistool.ErrCacher[api.AgentToken]{
				Log:          a.Log,
				ErrRep:       errRep,
				Client:       redisClient,
				ErrMarshaler: prototool.ProtoErrMarshaler{},
				KeyToRedisKey: func(key api.AgentToken) string {
					tokenHash := api.AgentToken2key(key)
					tokenHashStr := base64.StdEncoding.EncodeToString(tokenHash)
					return a.Configuration.Redis.KeyPrefix + ":agent_info_errs:" + tokenHashStr
				},
			},
			dt,
			gapi.IsCacheableError,
		),
	}
	return f.New, fAgent.New
}

func (a *ConfiguredApp) constructAgentTracker(errRep errz.ErrReporter, redisClient rueidis.Client, m otelmetric.Meter) (*agent_tracker.RedisTracker, error) {
	t, err := agent_tracker.NewRedisTracker(
		a.Log,
		errRep,
		redisClient,
		a.Configuration.Redis.KeyPrefix+":agent_tracker2",
		agent_registrar.RegisterAttemptInterval*3, // TTL
		agent_registrar.RegisterAttemptInterval*2, // GC
		m,
	)
	return t, err
}

func (a *ConfiguredApp) constructSentryHub(ot *obsTools) (*sentry.Hub, error) {
	s := a.Configuration.Observability.Sentry
	dialer := net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
	}
	sentryClient, err := sentry.NewClient(sentry.ClientOptions{
		Dsn:         s.Dsn, // empty DSN disables Sentry transport
		SampleRate:  1,     // no sampling
		Release:     cmd.Version,
		Environment: s.Environment,
		HTTPTransport: otelhttp.NewTransport(
			&http.Transport{
				Proxy:                 http.ProxyFromEnvironment,
				DialContext:           dialer.DialContext,
				TLSClientConfig:       tlstool.DefaultClientTLSConfig(),
				MaxIdleConns:          10,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ResponseHeaderTimeout: 20 * time.Second,
				ForceAttemptHTTP2:     true,
			},
			otelhttp.WithPropagators(ot.p),
			otelhttp.WithTracerProvider(ot.tp),
			otelhttp.WithMeterProvider(ot.mp),
		),
	})
	if err != nil {
		return nil, err
	}
	return sentry.NewHub(sentryClient, sentry.NewScope()), nil
}

func (a *ConfiguredApp) loadGitLabClientAuthSecret() ([]byte, error) {
	decodedAuthSecret, err := ioz.LoadBase64Secret(a.Configuration.Gitlab.AuthenticationSecretFile)
	if err != nil {
		return nil, fmt.Errorf("read file: %w", err)
	}
	if len(decodedAuthSecret) != authSecretLength {
		return nil, fmt.Errorf("decoding: expecting %d bytes, was %d", authSecretLength, len(decodedAuthSecret))
	}
	return decodedAuthSecret, nil
}

func (a *ConfiguredApp) constructGitLabClient(ot *obsTools) (*gitlab.Client, error) {
	cfg := a.Configuration

	gitLabURL, err := url.Parse(cfg.Gitlab.Address)
	if err != nil {
		return nil, err
	}
	// TLS cert for talking to GitLab/Workhorse.
	clientTLSConfig, err := tlstool.DefaultClientTLSConfigWithCACert(cfg.Gitlab.CaCertificateFile)
	if err != nil {
		return nil, err
	}
	// Secret for JWT signing
	decodedAuthSecret, err := a.loadGitLabClientAuthSecret()
	if err != nil {
		return nil, fmt.Errorf("authentication secret: %w", err)
	}
	var limiter httpz.Limiter
	limiter = rate.NewLimiter(
		rate.Limit(cfg.Gitlab.ApiRateLimit.RefillRatePerSecond),
		int(cfg.Gitlab.ApiRateLimit.BucketSize),
	)
	limiter, err = metric.NewWaitLimiterInstrumentation(
		"gitlab_client",
		cfg.Gitlab.ApiRateLimit.RefillRatePerSecond,
		"{refill/s}",
		ot.tracer,
		ot.meter,
		limiter,
	)
	if err != nil {
		return nil, err
	}
	return gitlab.NewClient(
		gitLabURL,
		decodedAuthSecret,
		gitlab.WithTextMapPropagator(ot.p),
		gitlab.WithTracerProvider(ot.tp),
		gitlab.WithMeterProvider(ot.mp),
		gitlab.WithUserAgent(kasServerName()),
		gitlab.WithTLSConfig(clientTLSConfig),
		gitlab.WithRateLimiter(limiter),
	), nil
}

func (a *ConfiguredApp) constructGitalyPool(ot *obsTools) (*client.Pool, error) {
	g := a.Configuration.Gitaly
	var globalGitalyRPCLimiter grpctool.ClientLimiter
	globalGitalyRPCLimiter = rate.NewLimiter(
		rate.Limit(g.GlobalApiRateLimit.RefillRatePerSecond),
		int(g.GlobalApiRateLimit.BucketSize),
	)
	globalGitalyRPCLimiter, err := metric.NewWaitLimiterInstrumentation(
		"gitaly_client_global",
		g.GlobalApiRateLimit.RefillRatePerSecond,
		"{refill/s}",
		ot.tracer,
		ot.meter,
		globalGitalyRPCLimiter,
	)
	if err != nil {
		return nil, err
	}
	return client.NewPoolWithOptions(
		client.WithDialOptions(
			grpc.WithUserAgent(kasServerName()),
			grpc.WithStatsHandler(ot.csh),
			grpc.WithStatsHandler(otelgrpc.NewClientHandler(
				otelgrpc.WithTracerProvider(ot.tp),
				otelgrpc.WithMeterProvider(ot.mp),
				otelgrpc.WithPropagators(ot.p),
				otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
			)),
			grpc.WithSharedWriteBuffer(true),
			// In https://gitlab.com/groups/gitlab-org/-/epics/8971, we added DNS discovery support to Praefect. This was
			// done by making two changes:
			// - Configure client-side round-robin load-balancing in client dial options. We added that as a default option
			// inside gitaly client in gitaly client since v15.9.0
			// - Configure DNS resolving. Due to some technical limitations, we don't use gRPC's built-in DNS resolver.
			// Instead, we implement our own DNS resolver. This resolver is exposed via the following configuration.
			// Afterward, workhorse can detect and handle DNS discovery automatically. The user needs to setup and set
			// Gitaly address to something like "dns:gitaly.service.dc1.consul"
			client.WithGitalyDNSResolver(client.DefaultDNSResolverBuilderConfig()),
			// Don't put interceptors here as order is important. Put them below.
		),
		client.WithDialer(func(ctx context.Context, address string, dialOptions []grpc.DialOption) (*grpc.ClientConn, error) {
			var perServerGitalyRPCLimiter grpctool.ClientLimiter
			perServerGitalyRPCLimiter = rate.NewLimiter(
				rate.Limit(g.PerServerApiRateLimit.RefillRatePerSecond),
				int(g.PerServerApiRateLimit.BucketSize))
			perServerGitalyRPCLimiter, err := metric.NewWaitLimiterInstrumentation( //nolint: contextcheck
				"gitaly_client_"+address,
				g.GlobalApiRateLimit.RefillRatePerSecond,
				"{refill/s}",
				ot.tracer,
				ot.meter,
				perServerGitalyRPCLimiter,
			)
			if err != nil {
				return nil, err
			}
			opts := []grpc.DialOption{
				grpc.WithChainStreamInterceptor(
					ot.streamClientProm,
					grpctool.StreamClientLimitingInterceptor(globalGitalyRPCLimiter),
					grpctool.StreamClientLimitingInterceptor(perServerGitalyRPCLimiter),
				),
				grpc.WithChainUnaryInterceptor(
					ot.unaryClientProm,
					grpctool.UnaryClientLimitingInterceptor(globalGitalyRPCLimiter),
					grpctool.UnaryClientLimitingInterceptor(perServerGitalyRPCLimiter),
				),
			}
			opts = append(opts, dialOptions...)
			return client.DialContext(ctx, address, opts)
		}),
	), nil
}

func (a *ConfiguredApp) constructRedisClient(ot *obsTools) (rueidis.Client, error) {
	cfg := a.Configuration.Redis
	dialTimeout := cfg.DialTimeout.AsDuration()
	writeTimeout := cfg.WriteTimeout.AsDuration()
	var err error
	var tlsConfig *tls.Config
	if cfg.Tls != nil && cfg.Tls.Enabled {
		tlsConfig, err = tlstool.DefaultClientTLSConfigWithCACertKeyPair(cfg.Tls.CaCertificateFile, cfg.Tls.CertificateFile, cfg.Tls.KeyFile)
		if err != nil {
			return nil, err
		}
	}
	var password string
	switch {
	case cfg.PasswordFile != "" && cfg.Password != "":
		return nil, errors.New("only one of password or password_file can be specified")
	case cfg.PasswordFile != "":
		passwordBytes, err := os.ReadFile(cfg.PasswordFile)
		if err != nil {
			return nil, err
		}
		password = string(passwordBytes)
	case cfg.Password != "":
		password = cfg.Password
	}
	opts := rueidis.ClientOption{
		Dialer: net.Dialer{
			Timeout: dialTimeout,
		},
		TLSConfig:         tlsConfig,
		Username:          cfg.Username,
		Password:          password,
		ClientName:        kasName,
		ConnWriteTimeout:  writeTimeout,
		DisableCache:      true,
		SelectDB:          int(cfg.DatabaseIndex),
		PipelineMultiplex: int(cfg.PipelineMultiplex),
	}
	if cfg.Network == "unix" {
		opts.DialFn = redistool.UnixDialer
	}
	switch v := cfg.RedisConfig.(type) {
	case *kascfg.RedisCF_Server:
		opts.InitAddress = []string{v.Server.Address}
		if opts.TLSConfig != nil {
			opts.TLSConfig.ServerName, _, _ = strings.Cut(v.Server.Address, ":")
		}
	case *kascfg.RedisCF_Sentinel:
		opts.InitAddress = v.Sentinel.Addresses
		var sentinelPassword string
		if v.Sentinel.SentinelPasswordFile != "" {
			sentinelPasswordBytes, err := os.ReadFile(v.Sentinel.SentinelPasswordFile)
			if err != nil {
				return nil, err
			}
			sentinelPassword = string(sentinelPasswordBytes)
		}
		opts.Sentinel = rueidis.SentinelOption{
			Dialer:    opts.Dialer,
			TLSConfig: opts.TLSConfig,
			MasterSet: v.Sentinel.MasterName,
			Username:  cfg.Username,
			Password:  sentinelPassword,
		}
	default:
		// This should never happen
		return nil, fmt.Errorf("unexpected Redis config type: %T", cfg.RedisConfig)
	}
	// Instrument Redis client with tracing and metrics.
	return rueidisotel.NewClient(opts,
		rueidisotel.WithTracerProvider(ot.tp),
		rueidisotel.WithMeterProvider(ot.mp),
	)
}

func constructTracingExporter(ctx context.Context, otlpEndpoint, otlpCaCertificateFile, otlpTokenSecretFile string) (tracesdk.SpanExporter, error) {
	u, err := url.Parse(otlpEndpoint)
	if err != nil {
		return nil, fmt.Errorf("parsing tracing url %s failed: %w", otlpEndpoint, err)
	}

	var otlpOptions []otlptracehttp.Option

	switch u.Scheme {
	case "https":
	case "http":
		otlpOptions = append(otlpOptions, otlptracehttp.WithInsecure())
	default:
		return nil, fmt.Errorf("unsupported schema of tracing url %q, only `http` and `https` are permitted", u.Scheme)
	}

	otlpOptions = append(otlpOptions, otlptracehttp.WithEndpoint(u.Host))
	otlpOptions = append(otlpOptions, otlptracehttp.WithURLPath(u.Path))

	if otlpTokenSecretFile != "" {
		token, err := os.ReadFile(otlpTokenSecretFile) //nolint: gosec, govet
		if err != nil {
			return nil, fmt.Errorf("unable to read OTLP token from %q: %w", otlpTokenSecretFile, err)
		}
		token = bytes.TrimSpace(token)
		headers := map[string]string{
			"Private-Token": string(token),
		}

		otlpOptions = append(otlpOptions, otlptracehttp.WithHeaders(headers))
	}

	tlsConfig, err := tlstool.DefaultClientTLSConfigWithCACert(otlpCaCertificateFile)
	if err != nil {
		return nil, err
	}
	otlpOptions = append(otlpOptions, otlptracehttp.WithTLSClientConfig(tlsConfig))

	return otlptracehttp.New(ctx, otlpOptions...)
}

func (a *ConfiguredApp) constructOTELMeterProvider(r *resource.Resource, reg prometheus.Registerer) (*metricsdk.MeterProvider, func() error, error) {
	otelPromExp, err := promexp.New(promexp.WithRegisterer(reg))
	if err != nil {
		return nil, nil, err
	}
	mp := metricsdk.NewMeterProvider(metricsdk.WithReader(otelPromExp), metricsdk.WithResource(r))
	mpStop := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		return mp.Shutdown(ctx)
	}
	return mp, mpStop, nil
}

func (a *ConfiguredApp) constructOTELTracingTools(ctx context.Context, r *resource.Resource) (trace.TracerProvider, propagation.TextMapPropagator, func() error, error) {
	if !a.isTracingEnabled() {
		return noop.NewTracerProvider(), propagation.NewCompositeTextMapPropagator(), func() error { return nil }, nil
	}

	// Exporter must be constructed right before TracerProvider as it's started implicitly so needs to be stopped,
	// which TracerProvider does in its Shutdown() method.
	mainExporter, err := constructTracingExporter(
		ctx,
		a.Configuration.Observability.Tracing.GetOtlpEndpoint(),
		a.Configuration.Observability.Tracing.GetOtlpCaCertificateFile(),
		a.Configuration.Observability.Tracing.GetOtlpTokenSecretFile(),
	)
	if err != nil {
		return nil, nil, nil, err
	}

	var exporter tracesdk.SpanExporter

	if a.Configuration.Observability.Tracing.GetOtlpLegacyEndpoint() == "" {
		exporter = mainExporter
	} else {
		// NOTE(prozlach): This is only a temporary measure to enable smooth
		// migration from the old observability stack to the new one.
		// The idea is to dual-write traces to both old and new stack. Once the
		// new stack is mature enough, we remove dual-tracing.
		legacyExporter, err := constructTracingExporter(
			ctx,
			a.Configuration.Observability.Tracing.GetOtlpLegacyEndpoint(),
			a.Configuration.Observability.Tracing.GetOtlpCaCertificateFile(),
			a.Configuration.Observability.Tracing.GetOtlpTokenSecretFile(),
		)
		if err != nil {
			return nil, nil, nil, err
		}

		exporter = &TraceDualWriter{
			mainExporter:   mainExporter,
			legacyExporter: legacyExporter,
		}
	}

	tp := tracesdk.NewTracerProvider(
		tracesdk.WithResource(r),
		tracesdk.WithBatcher(exporter),
	)
	p := propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{})
	tpStop := func() error { //nolint:contextcheck
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		return tp.Shutdown(ctx)
	}
	return tp, p, tpStop, nil
}

func (a *ConfiguredApp) isTracingEnabled() bool {
	return a.Configuration.Observability.Tracing != nil
}

func (a *ConfiguredApp) constructObservabilityTools(ctx context.Context) (ot *obsTools, stop func() error, retErr error) {
	// Metrics
	reg := prometheus.NewPedanticRegistry()
	goCollector := collectors.NewGoCollector()
	procCollector := collectors.NewProcessCollector(collectors.ProcessCollectorOpts{})
	srvProm := grpc_prometheus.NewServerMetrics()
	clientProm := grpc_prometheus.NewClientMetrics()
	err := metric.Register(reg, goCollector, procCollector, srvProm, clientProm)
	if err != nil {
		return nil, nil, err
	}

	// OTEL resource
	r, err := constructOTELResource()
	if err != nil {
		return nil, nil, err
	}

	// OTEL metrics
	mp, mpStop, err := a.constructOTELMeterProvider(r, reg) //nolint: contextcheck
	if err != nil {
		return nil, nil, err
	}
	defer callOnError(mpStop, &retErr)
	dm := mp.Meter(kasMeterName)
	ssh, err := grpctool.NewServerRequestsInFlightStatsHandler(dm)
	if err != nil {
		return nil, nil, err
	}
	csh, err := grpctool.NewClientRequestsInFlightStatsHandler(dm)
	if err != nil {
		return nil, nil, err
	}
	err = gitlabBuildInfoGauge(dm) //nolint: contextcheck
	if err != nil {
		return nil, nil, err
	}

	// OTEL Tracing
	tp, p, tpStop, err := a.constructOTELTracingTools(ctx, r)
	if err != nil {
		return nil, nil, err
	}
	defer callOnError(tpStop, &retErr)

	return &obsTools{
			probeRegistry:    observability.NewProbeRegistry(),
			reg:              reg,
			tracer:           tp.Tracer(kasTracerName),
			meter:            dm,
			tp:               tp,
			mp:               mp,
			p:                p,
			ssh:              ssh,
			csh:              csh,
			streamProm:       srvProm.StreamServerInterceptor(),
			unaryProm:        srvProm.UnaryServerInterceptor(),
			streamClientProm: clientProm.StreamClientInterceptor(),
			unaryClientProm:  clientProm.UnaryClientInterceptor(),
		}, func() error {
			return errors.Join(
				mpStop(),
				tpStop(),
			)
		}, nil
}

func callOnError(f func() error, err *error) {
	if *err != nil {
		_ = f()
	}
}

func startModules(stage stager.Stage, modules []modserver.Module) {
	for _, module := range modules {
		module := module // closure captures the right variable
		stage.Go(func(ctx context.Context) error {
			err := module.Run(ctx)
			if err != nil {
				return fmt.Errorf("%s: %w", module.Name(), err)
			}
			return nil
		})
	}
}

func constructRedisReadinessProbe(redisClient rueidis.Client) observability.Probe {
	return func(ctx context.Context) error {
		pingCmd := redisClient.B().Ping().Build()
		err := redisClient.Do(ctx, pingCmd).Error()
		if err != nil {
			return fmt.Errorf("redis: %w", err)
		}
		return nil
	}
}

func constructOTELResource() (*resource.Resource, error) {
	return resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(kasName),
			semconv.ServiceVersion(cmd.Version),
		),
	)
}

type connectedAgentInfoMarshaler agent_tracker.ConnectedAgentInfo

func (i *connectedAgentInfoMarshaler) MarshalJSON() ([]byte, error) {
	return protojson.Marshal((*agent_tracker.ConnectedAgentInfo)(i))
}

type redisAgentInfoResolver struct {
	agentQuerier agent_tracker.Querier
}

func (r redisAgentInfoResolver) Get(ctx context.Context, agentID int64) (map[string]interface{}, error) {
	var infos []*connectedAgentInfoMarshaler
	err := r.agentQuerier.GetConnectionsByAgentID(ctx, agentID, func(i *agent_tracker.ConnectedAgentInfo) (bool, error) {
		infos = append(infos, (*connectedAgentInfoMarshaler)(i))
		return false, nil
	})
	if err != nil {
		return nil, err
	}

	return map[string]interface{}{
		"agents": infos,
	}, nil
}

func constructAgentInfoResolver(agentQuerier agent_tracker.Querier) modserver.AgentInfoResolver {
	return redisAgentInfoResolver{agentQuerier: agentQuerier}
}

type agentFinder struct {
	agentQuerier agent_tracker.Querier
}

func (a *agentFinder) AgentLastConnected(ctx context.Context, agentID int64) (time.Time, error) {
	lastConnectedAt := time.Time{}
	err := a.agentQuerier.GetConnectionsByAgentID(ctx, agentID, func(i *agent_tracker.ConnectedAgentInfo) (bool, error) {
		if t := i.ConnectedAt.AsTime(); t.After(lastConnectedAt) {
			lastConnectedAt = t
		}
		return false, nil
	})
	if err != nil {
		return time.Time{}, err
	}

	return lastConnectedAt, nil
}

func constructAgentFinder(agentQuerier agent_tracker.Querier) *agentFinder {
	return &agentFinder{agentQuerier: agentQuerier}
}

func gitlabBuildInfoGauge(m otelmetric.Meter) error {
	g, err := m.Int64Gauge(gitlabBuildInfoGaugeMetricName, otelmetric.WithDescription("Current build info for this GitLab Service"))
	if err != nil {
		return err
	}
	g.Record(context.Background(), 1, otelmetric.WithAttributeSet(attribute.NewSet(kasVersionAttr.String(cmd.Version))))
	return nil
}

func kasServerName() string {
	return fmt.Sprintf("%s/%s/%s", kasName, cmd.Version, cmd.Commit)
}

var (
	_ redistool.RPCAPI = (*tokenLimiterAPI)(nil)
)

type tokenLimiterAPI struct {
	rpcAPI modserver.AgentRPCAPI
}

func (a *tokenLimiterAPI) Log() *zap.Logger {
	return a.rpcAPI.Log()
}

func (a *tokenLimiterAPI) HandleProcessingError(msg string, err error) {
	a.rpcAPI.HandleProcessingError(a.rpcAPI.Log(), msg, err)
}

func (a *tokenLimiterAPI) RequestKey() []byte {
	return api.AgentToken2key(a.rpcAPI.AgentToken())
}

var (
	_ grpctool.ServerErrorReporter = (*serverErrorReporter)(nil)
)

// serverErrorReporter implements the grpctool.ServerErrorReporter interface
// in order to report unknown grpc status code errors.
// In this case the errz.ErrReporter is used as a proxy for the modshared.RPCAPI,
// which logs and captures errors in Sentry.
type serverErrorReporter struct {
	log         *zap.Logger
	errReporter errz.ErrReporter
}

func (r *serverErrorReporter) Report(ctx context.Context, fullMethod string, err error) {
	r.errReporter.HandleProcessingError(ctx, r.log, fmt.Sprintf("Unknown gRPC error in %q", fullMethod), err)
}
