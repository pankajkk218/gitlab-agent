package kasapp

import (
	"context"
	"encoding/json"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
	"go.uber.org/mock/gomock"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// Test for https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/374.
// Test for https://github.com/open-telemetry/opentelemetry-go/issues/3769.
// If it fails, make sure you've updated the version of go.opentelemetry.io/otel/semconv that's imported to match
// what OTEL SDK uses internally.
func TestConstructResource(t *testing.T) {
	_, err := constructOTELResource()
	require.NoError(t, err)
}

func TestRedisAgentInfoResolver(t *testing.T) {
	ctrl := gomock.NewController(t)
	m := mock_agent_tracker.NewMockTracker(ctrl)
	m.EXPECT().
		GetConnectionsByAgentID(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		DoAndReturn(func(ctx context.Context, agentID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					Version:      "v1.0.0",
					CommitId:     "commit-id",
					PodNamespace: "agentk-pod-namespace",
					PodName:      "agentk-pod-name",
					KubernetesVersion: &entity.KubernetesVersion{
						Major:      "1",
						Minor:      "2",
						GitVersion: "3",
						Platform:   "4",
					},
				},
				ConnectedAt:  timestamppb.New(time.Unix(42, 24)),
				ConnectionId: 234234,
				AgentId:      testhelpers.AgentID,
				ProjectId:    testhelpers.ProjectID,
			})
			return err
		})
	r := redisAgentInfoResolver{
		agentQuerier: m,
	}
	data, err := r.Get(context.Background(), testhelpers.AgentID)
	require.NoError(t, err)

	b, err := json.Marshal(data)
	require.NoError(t, err)

	assert.Equal(t, `{"agents":[{"agent_meta":{"version":"v1.0.0","commit_id":"commit-id","pod_namespace":"agentk-pod-namespace","pod_name":"agentk-pod-name","kubernetes_version":{"major":"1","minor":"2","git_version":"3","platform":"4"}},"connected_at":"1970-01-01T00:00:42.000000024Z","connection_id":"234234","agent_id":"123","project_id":"321"}]}`, string(b))
}

func TestAgentFinder_AgentLastConnected_Error(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	m := mock_agent_tracker.NewMockTracker(ctrl)
	m.EXPECT().
		GetConnectionsByAgentID(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		Return(errors.New("dummy"))

	// WHEN
	af := agentFinder{
		agentQuerier: m,
	}
	actualLastConnectedAt, err := af.AgentLastConnected(context.Background(), testhelpers.AgentID)

	// THEN
	assert.Error(t, err)
	assert.Equal(t, time.Time{}, actualLastConnectedAt)
}

func TestAgentFinder_AgentLastConnected_NoAgents(t *testing.T) {
	// GIVEN
	ctrl := gomock.NewController(t)
	m := mock_agent_tracker.NewMockTracker(ctrl)
	m.EXPECT().
		GetConnectionsByAgentID(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		Return(nil)

	// WHEN
	af := agentFinder{
		agentQuerier: m,
	}
	actualLastConnectedAt, err := af.AgentLastConnected(context.Background(), testhelpers.AgentID)

	// THEN
	require.NoError(t, err)
	assert.Equal(t, time.Time{}, actualLastConnectedAt)
}

func TestAgentFinder_AgentLastConnected(t *testing.T) {
	// GIVEN
	expectedLastConnectedAt := time.Now().UTC()

	ctrl := gomock.NewController(t)
	m := mock_agent_tracker.NewMockTracker(ctrl)
	m.EXPECT().
		GetConnectionsByAgentID(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		DoAndReturn(func(ctx context.Context, agentID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					Version:      "v1.0.0",
					CommitId:     "commit-id",
					PodNamespace: "agentk-pod-namespace",
					PodName:      "agentk-pod-name",
					KubernetesVersion: &entity.KubernetesVersion{
						Major:      "1",
						Minor:      "2",
						GitVersion: "3",
						Platform:   "4",
					},
				},
				ConnectedAt:  timestamppb.New(expectedLastConnectedAt),
				ConnectionId: 234234,
				AgentId:      testhelpers.AgentID,
				ProjectId:    testhelpers.ProjectID,
			})
			return err
		})

	// WHEN
	af := agentFinder{
		agentQuerier: m,
	}
	actualLastConnectedAt, err := af.AgentLastConnected(context.Background(), testhelpers.AgentID)

	// THEN
	require.NoError(t, err)
	assert.Equal(t, expectedLastConnectedAt, actualLastConnectedAt)
}

func TestAgentFinder_AgentLastConnected_MultipleReplicas(t *testing.T) {
	// GIVEN
	pod1ConnectedAt := time.Now().UTC().Add(-1 * time.Minute)
	pod2ConnectedAt := time.Now().UTC().Add(+1 * time.Minute)
	expectedLastConnectedAt := pod2ConnectedAt

	ctrl := gomock.NewController(t)
	m := mock_agent_tracker.NewMockTracker(ctrl)
	m.EXPECT().
		GetConnectionsByAgentID(gomock.Any(), testhelpers.AgentID, gomock.Any()).
		DoAndReturn(func(ctx context.Context, agentID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					Version:      "v1.0.0",
					CommitId:     "commit-id",
					PodNamespace: "agentk-pod-namespace",
					PodName:      "agentk-pod-name-1",
					KubernetesVersion: &entity.KubernetesVersion{
						Major:      "1",
						Minor:      "2",
						GitVersion: "3",
						Platform:   "4",
					},
				},
				ConnectedAt:  timestamppb.New(pod1ConnectedAt),
				ConnectionId: 234234,
				AgentId:      testhelpers.AgentID,
				ProjectId:    testhelpers.ProjectID,
			})
			if err != nil {
				return err
			}
			_, err = cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					Version:      "v1.0.0",
					CommitId:     "commit-id",
					PodNamespace: "agentk-pod-namespace",
					PodName:      "agentk-pod-name-2",
					KubernetesVersion: &entity.KubernetesVersion{
						Major:      "1",
						Minor:      "2",
						GitVersion: "3",
						Platform:   "4",
					},
				},
				ConnectedAt:  timestamppb.New(pod2ConnectedAt),
				ConnectionId: 234235,
				AgentId:      testhelpers.AgentID,
				ProjectId:    testhelpers.ProjectID,
			})
			return err
		})

	// WHEN
	af := agentFinder{
		agentQuerier: m,
	}
	actualLastConnectedAt, err := af.AgentLastConnected(context.Background(), testhelpers.AgentID)

	// THEN
	require.NoError(t, err)
	assert.Equal(t, expectedLastConnectedAt, actualLastConnectedAt)
}
